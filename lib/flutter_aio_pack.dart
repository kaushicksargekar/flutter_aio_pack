library flutter_aio_pack;

export 'package:sized_context/sized_context.dart';
export 'package:retrofit/dio.dart';
export 'package:async_loader/async_loader.dart';
export 'package:flushbar/flushbar.dart';
export 'package:auto_size_text/auto_size_text.dart';
export 'package:flutter_spinkit/flutter_spinkit.dart';
export 'package:prefs_guard/prefsguard.dart';
export 'package:auto_route/auto_route.dart';
export 'package:auto_route/auto_route_annotations.dart';
export 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
export 'package:validators/validators.dart';
export 'package:validators/sanitizers.dart';
export 'package:flutter_icons/flutter_icons.dart';
export 'package:flutter_svg/flutter_svg.dart';
export 'package:velocity_x/velocity_x.dart';

export 'src/functions.dart';
export 'src/router_functions.dart';
export 'src/camera_cropper.dart';
export 'src/debounce.dart';
