import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_aio_pack/flutter_aio_pack.dart';
import 'package:logger/logger.dart';

void wtfL(val) => Logger().wtf(val);

void infoL(val) => Logger().i(val);

void errorL(val) => Logger().e(val);

void warningL(val) => Logger().w(val);

void debugL(val) => Logger().d(val);

void verboseL(val) => Logger().v(val);

void hideKeyboard() => SystemChannels.textInput.invokeMethod('TextInput.hide');

//development..
showFlushBar({
  @required BuildContext context,
  int duration: 1000,
  position: FlushbarPosition.BOTTOM,
  @required String content,
  color: Colors.greenAccent,
}) {
  return Flushbar(
    duration: duration.milliseconds,
    borderRadius: 10,
    margin: EdgeInsets.only(
        left: 10,
        right: 10,
        bottom: 30,
        top: position == FlushbarPosition.TOP ? 20 : 0),
    flushbarPosition: position,
    backgroundColor: color,
    flushbarStyle: FlushbarStyle.FLOATING,
    forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
    boxShadows: [
      BoxShadow(
        color: Colors.black54,
        offset: Offset(4, 9),
        blurRadius: 7,
      ),
    ],
    messageText: Align(
      alignment: Alignment.center,
      child: Text(
        content,
        style: TextStyle(
            color: Colors.white,
            fontFamily: 'QSM',
            fontSize: 22,
            letterSpacing: 2),
      ),
    ),
  )..show(context);
}
