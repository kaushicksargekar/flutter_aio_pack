import 'package:auto_route/auto_route.dart';


void push(String className)=>ExtendedNavigator.root.push(className);

void popPush(String className) => ExtendedNavigator.root.popAndPush(className);
void popUntil(String className) =>
    ExtendedNavigator.root.popUntil((route) => false);
void popAndPush(String className) =>
    ExtendedNavigator.root.popAndPush(className);