import 'dart:io';

import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

takeImageFromCamera() async {
  final _imagePicker = ImagePicker();
  final _image = await _imagePicker.getImage(source: ImageSource.camera);
  if (_image != null) {
    return _image.path;
  } else {
    return null;
  }
}

cropImageFromCamera() async {
  final _image = await takeImageFromCamera();
  if (_image != null) {
    File _file = await ImageCropper.cropImage(sourcePath: _image);
    if (_file != null) {
      return _file;
    } else {
      return null;
    }
  } else {
    return null;
  }
}

cropImageFromGallery() async {
  final _imagePicker = ImagePicker();
  final _image = await _imagePicker.getImage(source: ImageSource.gallery);
  if (_image != null) {
    File _file = await ImageCropper.cropImage(sourcePath: _image.path);
    if (_file != null) {
      return _file;
    } else {
      return null;
    }
  } else {
    return null;
  }
}
